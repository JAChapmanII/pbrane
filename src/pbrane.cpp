#include <iostream>
using std::cin;
using std::cout;
using std::cerr;
using std::endl;

#include <string>
using std::string;
using std::getline;

#include <vector>
using std::vector;

#include <random>
using std::random_device;

#include "global.hpp"
using global::isOwner;
using global::send;
#include "config.hpp"
#include "modules.hpp"
#include "util.hpp"
using util::contains;
using util::fromString;
using util::asString;
using util::split;
using util::startsWith;
#include "markov.hpp"
#include "eventsystem.hpp"
#include "expressiontree.hpp"
#include "events.hpp"

void process(string script, string nick, string target);
string evaluate(string script, string nick);


typedef bool (*hook)(string message, string nick, string target);

bool powerHook(string message, string nick, string target);
bool onHook(string message, string nick, string target);
bool inHook(string message, string nick, string target);
bool regexHook(string message, string nick, string target);

vector<hook> hooks = { &powerHook, &onHook, &inHook, &regexHook };


int main(int argc, char **argv) {
	unsigned int seed = 0;
	if(argc > 1) {
		seed = fromString<unsigned int>(argv[1]);
	} else {
		random_device randomDevice;
		seed = randomDevice();
	}

	if(!global::init(seed)) {
		cerr << "pbrane: global::init failed" << endl;
		return -1;
	}

	// TODO: don't hard-code these. These should be set in the startup file
	global::vars["bot.nick"] = "pbrane_future";
	global::vars["bot.maxLineLength"] = "256";
	global::vars["bot.owner"] = "jac";

	if(!global::secondaryInit()) {
		cerr << "pbrane: global::secondaryInit failed" << endl;
		// TODO: this should fail out completely?
	}


	global::log << "----- " << global::vars["bot.nick"].toString() << " started -----" << endl;
	cerr << "----- " << global::vars["bot.nick"].toString() << " started -----" << endl;

	modules::init(config::brainFileName);
	global::secondaryInit();

	// while there is more input coming
	int done = 0;
	while(!cin.eof() && !done) {
		// read the current line of input
		string line;
		getline(cin, line);

		// TODO: logging

		vector<string> fields = split(line);
		if(fields[1] == (string)"PRIVMSG") {
			string nick = fields[0].substr(1, fields[0].find("!") - 1);

			size_t mstart = line.find(":", 1);
			string message = line.substr(mstart + 1);

			string target = fields[2];
			if(fields[2] == global::vars["bot.nick"].toString())
				target = nick;

			// if the line is a ! command, run it
			if(message[0] == '!')
				process(message, nick, target);
			// if the line is a : invocation, evaluate it
			else if(message[0] == ':')
				process(message.substr(1), nick, target);
			// otherwise, run on text triggers
			else {
				// TODO: proper environment for triggers
				global::vars["nick"] = nick;
				global::vars["text"] = message;

				vector<Variable> results = global::eventSystem.process(EventType::Text, message);
				if(results.size() > 1)
					send(target, asString(results.size()) + " responses", true);
				else if(results.size() == 1)
					send(target, results.front().toString(), true);
			}
		}
		if(fields[1] == (string)"JOIN") {
			;// run join triggers
		}
		if(fields[1] == (string)"NICK") {
			;// run nick triggers
		}
		if((fields[1] == (string)"PART") || (fields[1] == (string)"QUIT")) {
			;// run leave triggers
		}
	}

	// free memory associated with modules
	modules::deinit(config::brainFileName);

	// deinit global
	global::deinit();

	return done - 1;
}

void process(string script, string nick, string target) {
	// run special hooks first
	bool processed = false;
	for(auto h : hooks)
		if((*h)(script, nick, target)) {
			processed = true;
			break;
		}
	if(processed)
		return;

	// assume we can run the script
	send(target, evaluate(script, nick), true);
}
string evaluate(string script, string nick) {
	string result;
	try {
		ExpressionTree *etree = ExpressionTree::parse(script);
		try {
			result = etree->evaluate(nick).toString();
		} catch(string &s) {
			result = nick + ": error: " + s;
		}
		delete etree;
	} catch(string &s) {
		result = nick + ": severe error: " + s;
	}
	return result;
}


bool powerHook(string message, string nick, string target) {
	if(message == (string)"!restart" && isOwner(nick))
		exit(0);
	return false;
}
bool onHook(string message, string nick, string target) {
	if(startsWith(message, "!on")) {
		vector<string> fs = split(message);
		if(fs.size() < 2) {
			send(target, nick + ": error: !on requires more arguments");
			return true;
		}
		vector<Variable> args;
		args.push_back(Variable(fs[1], Permissions(nick)));
		string script;
		for(unsigned i = 2; i < fs.size(); ++i)
			script += fs[i] + " ";
		args.push_back(Variable(script, Permissions(nick)));
		string result;
		try {
			Variable res = on(args);
			result = res.toString();
		} catch(string &s) {
			result = nick + ": error: " + s;
		}
		send(target, result, true);
		return true;
	}
	return false;
}
bool inHook(string message, string nick, string target) {
	return false;
}
bool regexHook(string message, string nick, string target) {
	return false;
}

